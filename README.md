# README #

* Original code was released in this youtube channel: [youtube channel](https://youtu.be/YRPMdb1YMS8)

### What is this repository for? ###

* A simple tool to scan and modify game memory
* Version 1.0

### Some common data sizes ###

* The size of an int: 4
* The size of a long int: 8
* The size of a short int: 2
* The size of a float: 4
* The size of a double: 8
* The size of a long double: 16
* The size of a char: 1
* The size of a bool: 1

### Examples of usage ###

![Alt example1](http://i.epvpimg.com/rGRsfab.png)